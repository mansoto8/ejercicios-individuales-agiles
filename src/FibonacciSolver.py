import math

__author__ = 'juan'

def calcularSecuenciaFibonacci(iteraciones):
    print("La secuencia fibonacci es la siguiente: ")
    print("1")
    prev1 = 0
    prev2 = 0
    cont = 1
    while cont < iteraciones :
        cont += 1
        if cont == 2:
            prev1 = 1
            prev2 = 1
            print(1)
        else:
            temp = prev2
            prev2 = prev1
            prev1 = prev1 + temp
            print(str(prev1))
    return

def solve():
    while True:
        try:
            iteraciones = int(input("Ingrese el numero de iteraciones a calcular para la secuencia fibonacci: "))

            if iteraciones<=0 :
                print 'Numero de iteraciones no valido. Debe ser mayor a cero. Vuelva a intentarlo.'
            else:
                print 'El numero de iteraciones ingresado fue: '+str(iteraciones)
                calcularSecuenciaFibonacci(iteraciones)
                break
        except :
            print "Numero no valido. Intenta de nuevo..."
    return

solve()

